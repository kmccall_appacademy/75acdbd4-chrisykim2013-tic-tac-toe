class HumanPlayer
  attr_accessor :name, :mark
  
  def initialize(name)
    @name = name
    @all_pos = [
      [0,0],[0,1],[0,2],
      [1,0],[1,1],[1,2],
      [2,0],[2,1],[2,2]
    ]
  end

  def get_move
    puts "#{@name}, where would you like to move? Please input in the form '0, 0'."
    pos_string = gets.chomp
    pos_arr = [pos_string[0].to_i, pos_string[3].to_i]
    if !@all_pos.include?(pos_arr)
      puts "Invalid response. Please try again."
      get_move
    elsif @all_pos.include?(pos_arr)
      pos_arr
    else
      false
    end
  end

  def display(board)
    puts "#{board.grid[0]}"
    puts "#{board.grid[1]}"
    puts "#{board.grid[2]}"
  end
end
