require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game
  attr_accessor :board, :mark

  def initialize(player_one, player_two)
    @player_one, @player_two = player_one, player_two
    player_one.mark , player_two.mark = :X, :O
    @board = Board.new
    @move_num = 0
    @current_player = @player_one
  end

  def play_turn
    current_player.display(@board)
    move = current_player.get_move
    # !!! Why can't my @board.id(move) work but the method below can???
    # Why can I use board and @board for the methods inside this method???
    if @board.place_mark(move, current_player.mark) == false
      puts 'Invalid response. Please try again.'
      play_turn
    else
      switch_players!
    end
  end

  def current_player
    @current_player = [@player_one, @player_two][@move_num % 2]
  end

  def switch_players!
    @move_num += 1
    current_player
  end

  def play
    until @board.over?
      play_turn
    end
    if @board.cats_game?
      puts "It is a tie!"
    else
      switch_players!
      puts "Congratulations #{@current_player.name}, you won the game!"
    end
  end
end

if __FILE__ == $PROGRAM_NAME
  puts "Would you like to play with another player (pvp) or with a computer (pvc)?"
  answer = gets.chomp

  if answer == "pvp"
    puts "Please give the name of player one."
    player_one = HumanPlayer.new(gets.chomp)
    puts "Please give the name of player two"
    player_two = HumanPlayer.new(gets.chomp)
    new_game = Game.new(player_one, player_two)
    new_game.play
  elsif answer == "pvc"
    puts "Please give the name of player one."
    player_one = HumanPlayer.new(gets.chomp)
    player_two = ComputerPlayer.new("Bot")
    new_game = Game.new(player_one, player_two)
    new_game.play
  else
    puts "Invalid choice. Please reset the game and try again."
  end
end
