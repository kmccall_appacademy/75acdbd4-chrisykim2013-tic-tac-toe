class Board
  def initialize(grid = [[nil, nil, nil],[nil, nil, nil],[nil, nil, nil]], turn = 0)
    @grid = grid
    @turn = turn
  end

  def grid
    @grid
  end

  def place_mark(pos, sym)
    if @grid[pos[0]][pos[1]] == nil
      @grid[pos[0]][pos[1]] = sym
      @turn += 1
    else
      false
    end
  end

  def empty?(pos)
    return true if @grid[pos[0]][pos[1]].nil?
  end

  def id(pos)
    @grid[pos[0]][pos[1]]
  end

  def winner
    return :X if row_win?(:X) || diag_win?(:X) || col_win?(:X)
    return :O if row_win?(:O) || diag_win?(:O) || col_win?(:O)
  end

  def row_win?(sym)
    return true if [[0, 0], [0, 1], [0, 2]].all? {|pos| id(pos) == sym}
    return true if [[1, 0], [1, 1], [1, 2]].all? {|pos| id(pos) == sym}
    return true if [[2, 0], [2, 1], [2, 2]].all? {|pos| id(pos) == sym}
    false
  end

  def diag_win?(sym)
    return true if [[0, 0], [1, 1], [2, 2]].all? {|pos| id(pos) == sym}
    return true if [[0, 2], [1, 1], [2, 0]].all? {|pos| id(pos) == sym}
    false
  end

  def col_win?(sym)
    return true if [[0, 0], [1, 0], [2, 0]].all? {|pos| id(pos) == sym}
    return true if [[0, 1], [1, 1], [2, 1]].all? {|pos| id(pos) == sym}
    return true if [[0, 2], [1, 2], [2, 2]].all? {|pos| id(pos) == sym}
    false
  end

  def cats_game?
    return true if winner == nil && @turn == 9
    false
  end

  def over?
    return true if winner == :X || winner == :O || cats_game?
    false
  end
end
