class ComputerPlayer
  attr_accessor :board, :mark, :name

  def initialize(name)
    @name = name
    @winning_comb = [
    [[0, 0], [0, 1], [0, 2]],
    [[1, 0], [1, 1], [1, 2]],
    [[2, 0], [2, 1], [2, 2]],
    [[0, 0], [1, 1], [2, 2]],
    [[0, 2], [1, 1], [2, 0]],
    [[0, 0], [1, 0], [2, 0]],
    [[0, 1], [1, 1], [2, 1]],
    [[0, 2], [1, 2], [2, 2]],
    ]
    @all_pos = [
      [0,0],[0,1],[0,2],
      [1,0],[1,1],[1,2],
      [2,0],[2,1],[2,2]
    ]
  end

  def display(board)
    @board = board
  end

  def get_move
    res_positions = []
    combination = pos.combination(2).to_a
    combination.each do |duo|
      @winning_comb.each do |comb|
        res_positions << comb if comb.include?(duo.first) && comb.include?(duo.last)
      end
    end
    res_positions.each {|comb| return winner(comb) unless winner(comb) == false}
    sample
  end

  def winner(arr_pos)
    arr_pos.each {|pos| return pos unless id(pos) != nil}
    false
  end

  def id(pos)
    @board.grid[pos[0]][pos[1]]
  end

  def sample
    samp = @all_pos.sample
    if id(samp) == nil
      return samp
    else
      sample
    end
  end

  def pos
    res = []
    shovel = []
    @board.grid.each.with_index do |row, i|
      next if !row.include?(@mark)
      shovel << i if row.include?(@mark)
      row.each.with_index do |symb, i2|
        next if symb != @mark
        shovel << i2
        res << shovel
        shovel = [i]
      end
      shovel = []
    end
    res
  end
end
